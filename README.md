Compose common services for web-applications development
=========================================================

Usage
-----

Get common services compose file:

```
wget https://bitbucket.org/minity/docker-webdev-base/raw/master/docker-compose.yml -O docker-compose-common.yml
```

Run *docker compose* with several --file parameters:

```
docker-compose \
        --file=your/project/docker-compose.yml \
        --file=docker-compose-common.yml \
        -p yourproject \
    up -d http-proxy adminer db yourservice1 yourservice2
```

Services in the project's `docker-compose.yml` file should be linked
with common services using `external_links` section.